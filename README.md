# ThinkDeep Test


## Instructions

Mise en place infra Kubernetes sur GCP

### Objectif: 

Mettre en place un cluster GCP avec quelques nodes sur lesquelles tournent un infra web "standard" : django + front JS + bdd postgresql.

Cette infra devra être monitorée avec Grafana/Prometheus afin d'avoir des dashboards de suivi.

Pas besoin d'aller trés loin sur la partie applicative, on s'attachera surtout au déploiement, monitoring et supervision du système.

Les fichiers source ainsi que la conf et yaml des déploiements seront versionnés dans Git.

Si tu peux utiliser un outil comme Terraform / Gitlab CI pour la conf du cluster et le déploiement c'est encore mieux.
